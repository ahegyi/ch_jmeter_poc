# ClickHouse JMeter Proof of concept

## About

This repository showcases a JMeter test plan which can be used to performance test ClickHouse databases via the HTTP interface.

## Setup

To try the test plan, install OpenJDK and download JMeter from https://jmeter.apache.org/download_jmeter.cgi

## Usage

Ensure that you have the latest schema in your ClickHouse database. For the Contribution Analytics feature, the `contribution_analytics_events` table should be present.

1. Clone the repository.
1. Configure the `gitlab_click_house.properties` with the correct ClickHouse credentials.
1. Start JMeter: `JMETER_PATH/bin/jmeter -p gitlab_click_house.properties -t gitlab_click_house.jmx`
1. Click the green play button.
1. Navigate to the `Aggregate Report` section (right side) and see the measurements.
1. Click STOP to stop the test.

![JMeter test plan](images/jmeter.png)

![JMeter aggregate report view](images/jmeter_aggregate_report.png)

## Test plan

The `gitlab_click_house.properties` file contain all GitLab (feature and test) specific configurations:

- credentials
- variables for generating query parameters
- request per minute configurations

When adding new test cases the properties file can be extended with more configurations. The test plan (.jmx file) will be loaded with the configuration when the tool starts. The right side contains the test plan components.

The top level component `Test plan for performance testing ClickHouse via HTTP` sets up the configuration variables from the `properties` file. The variables are used for templating the various settings within JMeter such as the host and port for the HTTP requests.

The test plan uses HTTP requests that are configured the same way as ClickHouse is configured within the GitLab application (headers, paths).

For now, we have two thread groups (test cases) which are testing the Contribution Analytics queries: weekly and monthly queries. The queries run with different throughput settings which mimics the production server load. For simplicity, we use constant throughput but this could be easily changed later on. The Contribution Analytics query requires 3 arguments:

- Start date
- End date
- Group path (example: `top_group1_id/subgroup_id/`)

The dates are generated on the fly using an inline groovy script (Contribution Analytics Common -> Data Generator). The group paths are read from the `properties` file: `click_house_contribution_analytics_group_paths` setting.

## Data generation

The test plan is not responsible for preparing test data in ClickHouse. This needs to be done with a separate script. Keep in mind that the script will have to be consistent with the `gitlab_click_house.properties` file. For example when generating data for 10 years, the `click_house_contribution_analytics_start_date` and `click_house_contribution_analytics_end_date` properties should match the generated date range. Same goes for the `click_house_contribution_analytics_group_paths` setting, ideally we want to pick a few dozen group paths from the generated data.

## Possible improvements

- Compose test plans from separate `.jmx` files so we can independently develop tests.
- Extract the groovy scripts from the test plan to separate files.
- Setup result logging and export.
